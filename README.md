[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.hrz.tu-chemnitz.de%2Frst%2Fpublic%2Fteaching%2Fflavert-nbviewer-content/binder_public)

# Jupyter-Notebooks zur Lehrveranstaltung Steuerung und Regelung flacher und verteiltparametrischer Systeme

Dieses Repositorium enthält Jupyter-Notebooks begleitend zur Lehrveranstaltung
Steuerung und Regelung flacher und verteiltparametrischer Systeme am [Institut für Regelungs- und
Steuerungstheorie](https://tu-dresden.de/ing/elektrotechnik/rst), TU Dresden.

Nachfolgend Links zur Ansicht der verfügbaren Notebooks (über [https://nbviewer.jupyter.org](https://nbviewer.jupyter.org),
keine eigene Python-Installation nötig, Notebooks werden aber nur statisch angezeigt. Für lauffähige Versionen siehe obiger mybinder-Link oder selbst runterladen und nutzen).
